package pizzashop.service.unit_tests;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class PizzaServiceUnitTest {

    @Mock
    private Payment payment;

    @Mock
    private PaymentRepository paymentRepository;

    @InjectMocks
    private PizzaService pizzaService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addPayment_validParameters_paymentAdded() {
        // Arrange
        doNothing().when(paymentRepository).add(any(Payment.class));
        // Act
        pizzaService.addPayment(4, PaymentType.CASH, 10);
        // Assert
        Mockito.verify(paymentRepository, times(1)).add(any(Payment.class));
        assert true;
    }

    @Test
    public void getTotalAmount_returnTotal() {
        // Arrange
        when(payment.getType()).thenReturn(PaymentType.CARD);
        when(payment.getAmount()).thenReturn(10.0d);
        List<Payment> paymentList = new ArrayList<>();
        paymentList.add(payment);
        paymentList.add(payment);
        when(paymentRepository.getAll()).thenReturn(paymentList);
        // Act
        // Assert
        assertEquals(20, pizzaService.getTotalAmount(PaymentType.CARD));
        Mockito.verify(paymentRepository, times(1)).getAll();
        Mockito.verify(payment, times(2)).getType();
        Mockito.verify(payment, times(2)).getAmount();
        assert true;
    }

}
