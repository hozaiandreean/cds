package pizzashop.service.unit_tests;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import pizzashop.model.Payment;
import pizzashop.repository.PaymentRepository;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class PaymentRepositoryUnitTest {
    @Mock
    private Payment payment;

    private PaymentRepository paymentRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        paymentRepository = new PaymentRepository();
    }

    @AfterEach
    public void deleteContentFile() {
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file));
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addPayment_validParameters_paymentAdded() {
        //Arrange
        Mockito.when(payment.toString()).thenReturn("1,CARD,10");
        //Act
        paymentRepository.add(payment);
        //Assert
        assertEquals(1, paymentRepository.getAll().size());
        assertEquals("1,CARD,10", paymentRepository.getAll().get(0).toString());

        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            assertEquals("1,CARD,10", line);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAllPayments_returnList() {
        //Arrange
        Mockito.when(payment.toString()).thenReturn("1,CARD,10");
        paymentRepository.add(payment);
        paymentRepository.add(payment);
        //Act
        //Assert
        assertEquals(2, paymentRepository.getAll().size());
        assertEquals("1,CARD,10", paymentRepository.getAll().get(0).toString());

        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            int count = 0;
            while (br.readLine() != null) {
                count++;
            }
            assertEquals(2, count);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}