package pizzashop.service.unit_tests;

import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import static org.junit.jupiter.api.Assertions.*;

public class PaymentUnitTest {
    @Test
    void createPayment_validParameters_returnsCreatedPayment() {
        //Arrange
        //Act
        Payment payment = new Payment(4, PaymentType.CASH, 2.35f);
        //Assert
        assertNotNull(payment);
        assertEquals(4, payment.getTableNumber());
        assertEquals(PaymentType.CASH, payment.getType());
        assertEquals(2.35f, payment.getAmount());
    }

    @Test
    void setPaymentParameters_validParameters_changesApplied() {
        //Arrange
        Payment payment = new Payment(4, PaymentType.CASH, 2.35f);
        //Act
        payment.setTableNumber(1);
        payment.setType(PaymentType.CARD);
        payment.setAmount(10.00f);
        //Assert
        assertEquals(1, payment.getTableNumber());
        assertEquals(PaymentType.CARD, payment.getType());
        assertEquals(10.00f, payment.getAmount());
    }
}
