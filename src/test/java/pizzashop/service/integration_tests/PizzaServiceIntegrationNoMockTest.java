package pizzashop.service.integration_tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PizzaServiceIntegrationNoMockTest {
    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private PizzaService pizzaServiceUnderTest;

    @BeforeEach
    public void init() {
        menuRepo = new MenuRepository();
        payRepo = new PaymentRepository();
        pizzaServiceUnderTest = new PizzaService(menuRepo, payRepo);
    }

    @AfterEach
    public void deleteContentFile() {
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file));
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void getTotalAmount_onePaymentWithGivenPaymentType_returnSum() {
        //Arrange
        payRepo.add(new Payment(3, PaymentType.CASH, 24));
        //Act
        //Assert
        assertEquals(24, pizzaServiceUnderTest.getTotalAmount(PaymentType.CASH));
        assertEquals(1, payRepo.getAll().size());

        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            assertEquals("3,CASH,24.0", line);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void addPayment_validAmount_doesNotThrowException() {
        //Arrange
        int tableNumber = 6;
        int amount = 23;
        PaymentType paymentType = PaymentType.CARD;
        //Act
        //Assert
        assertDoesNotThrow(() -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
        assertEquals(1, payRepo.getAll().size());

        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            assertEquals("6,CARD,23.0", line);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
