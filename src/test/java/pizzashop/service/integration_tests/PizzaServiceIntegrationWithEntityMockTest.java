package pizzashop.service.integration_tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PizzaServiceIntegrationWithEntityMockTest {
    @Mock
    private Payment payment;
    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private PizzaService pizzaServiceUnderTest;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        menuRepo = new MenuRepository();
        payRepo = new PaymentRepository();
        pizzaServiceUnderTest = new PizzaService(menuRepo, payRepo);
    }

    @AfterEach
    public void deleteContentFile() {
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file));
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void getTotalAmount_onePaymentWithGivenPaymentType_returnSum() {
        //Arrange
        when(payment.toString()).thenReturn("3,CASH,24.0");
        when(payment.getType()).thenReturn(PaymentType.CASH);
        when(payment.getAmount()).thenReturn(24.0d);
        payRepo.add(payment);
        //Act
        //Assert
        assertEquals(24, pizzaServiceUnderTest.getTotalAmount(PaymentType.CASH));
        assertEquals(1, payRepo.getAll().size());

        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            assertEquals("3,CASH,24.0", line);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        verify(payment, times(1)).getType();
        verify(payment, times(1)).getAmount();
        assert true;
    }

    @Test
    void getPayments_returnAllPayments() {
        //Arrange
        when(payment.toString()).thenReturn("3,CASH,24.0");
        payRepo.add(payment);
        payRepo.add(payment);
        //Act
        //Assert
        assertEquals(2, payRepo.getAll().size());

        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            int count = 0;
            while ((line = br.readLine()) != null) {
                count++;
                assertEquals("3,CASH,24.0", line);
            }
            assertEquals(2, count);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
