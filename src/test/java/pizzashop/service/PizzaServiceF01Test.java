package pizzashop.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PizzaServiceF01Test {
    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private PizzaService pizzaServiceUnderTest;

    @BeforeEach
    public void init() {
        menuRepo = new MenuRepository();
        payRepo = new PaymentRepository();
        pizzaServiceUnderTest = new PizzaService(menuRepo, payRepo);
    }

    @AfterEach
    public void deleteContentFile() {
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file));
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Disabled
    void addPayment_invalidTableNumberAndAmount_throwException() {
        //Arrange
        int tableNumber = 0;
        PaymentType paymentType = PaymentType.CARD;
        double amount = -7.99;
        //Act
        //Assert
        assertThrows(RuntimeException.class, () -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    //ECP Tests
    @Test
    @Order(1)
    @DisplayName(value = "Valid Table Number -> Does not throw exception")
    void addPayment_validTableNumber_doesNotThrowException() {
        //Arrange
        PaymentType paymentType = PaymentType.CARD;
        double amount = 10.5;
        int tableNumber = 5;
        //Act
        //Assert
        assertDoesNotThrow(() -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1})
    @Order(2)
    void addPayment_lowerTableNumber_throwException(int tableNumber) {
        //Arrange
        PaymentType paymentType = PaymentType.CARD;
        double amount = 10.5;
        //Act
        //Assert
        assertThrows(RuntimeException.class, () -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @Test
    @Order(3)
    void addPayment_greaterTableNumber_throwException() {
        //Arrange
        PaymentType paymentType = PaymentType.CARD;
        double amount = 10.5;
        int tableNumber = 10;
        //Act
        //Assert
        assertThrows(RuntimeException.class, () -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @ParameterizedTest
    @ValueSource(doubles = {23})
    @Order(4)
    void addPayment_validAmount_doesNotThrowException(double amount) {
        //Arrange
        int tableNumber = 6;
        PaymentType paymentType = PaymentType.CARD;
        //Act
        //Assert
        assertDoesNotThrow(() -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @Test
    @Order(5)
    void addPayment_negativeAmount_throwException() {
        //Arrange
        int tableNumber = 6;
        double amount = -1.21;
        PaymentType paymentType = PaymentType.CARD;
        //Act
        //Assert
        assertThrows(RuntimeException.class, () -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    //BVA Tests
    @ParameterizedTest
    @ValueSource(ints = {1})
    @Order(6)
    void addPayment_startEdgeTableNumber_doesNotThrowException(int tableNumber) {
        //Arrange
        PaymentType paymentType = PaymentType.CARD;
        double amount = 10.5;
        //Act
        //Assert
        assertDoesNotThrow(() -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @ParameterizedTest
    @ValueSource(ints = {8})
    @Order(7)
    void addPayment_endEdgeTableNumber_doesNotThrowException(int tableNumber) {
        //Arrange
        PaymentType paymentType = PaymentType.CARD;
        double amount = 10.5;
        //Act
        //Assert
        assertDoesNotThrow(() -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @Test
    @Order(8)
    void addPayment_belowStartEdgeTableNumber_throwException() {
        //Arrange
        PaymentType paymentType = PaymentType.CARD;
        double amount = 10.5;
        int tableNumber = 0;
        //Act
        //Assert
        assertThrows(RuntimeException.class, () -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @ParameterizedTest
    @ValueSource(ints = {9})
    @Order(9)
    void addPayment_aboveEndEdgeTableNumber_throwException(int tableNumber) {
        //Arrange
        PaymentType paymentType = PaymentType.CARD;
        double amount = 10.5;
        //Act
        //Assert
        assertThrows(RuntimeException.class, () -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @Test
    @Order(10)
    void addPayment_aboveStartEdgeTableNumber_doesNotThrowException() {
        //Arrange
        PaymentType paymentType = PaymentType.CARD;
        double amount = 10.5;
        int tableNumber = 2;
        //Act
        //Assert
        assertDoesNotThrow(() -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @ParameterizedTest
    @ValueSource(ints = {7})
    @Order(11)
    void addPayment_belowEndEdgeTableNumber_doesNotThrowException(int tableNumber) {
        //Arrange
        PaymentType paymentType = PaymentType.CARD;
        double amount = 10.5;
        //Act
        //Assert
        assertDoesNotThrow(() -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0})
    @Order(12)
    void addPayment_edgeAmount_doesNotThrowException(double amount) {
        //Arrange
        int tableNumber = 5;
        PaymentType paymentType = PaymentType.CARD;
        //Act
        //Assert
        assertThrows(RuntimeException.class, () -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @ParameterizedTest
    @ValueSource(doubles = {-0.01})
    @Order(13)
    void addPayment_belowEdgeAmount_throwException(double amount) {
        //Arrange
        int tableNumber = 5;
        PaymentType paymentType = PaymentType.CARD;
        //Act
        //Assert
        assertThrows(RuntimeException.class, () -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }

    @Test
    @Order(14)
    void addPayment_aboveEdgeAmount_doesNotThrowException() {
        //Arrange
        int tableNumber = 5;
        double amount = 0.01;
        PaymentType paymentType = PaymentType.CARD;
        //Act
        //Assert
        assertDoesNotThrow(() -> pizzaServiceUnderTest.addPayment(tableNumber, paymentType, amount));
    }
}