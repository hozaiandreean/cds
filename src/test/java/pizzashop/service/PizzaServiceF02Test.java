package pizzashop.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class PizzaServiceF02Test {
    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private PizzaService pizzaServiceUnderTest;

    @BeforeEach
    public void init() {
        menuRepo = new MenuRepository();
        payRepo = new PaymentRepository();
        pizzaServiceUnderTest = new PizzaService(menuRepo, payRepo);
    }

    @AfterEach
    public void deleteContentFile(){
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource("data/payments.txt").getFile());

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file));
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void getTotalAmount_payRepoIsNull_returnZero(){
        //Arrange
        pizzaServiceUnderTest = new PizzaService(menuRepo, null);
        //Act
        //Assert
        assertEquals(0.0f, pizzaServiceUnderTest.getTotalAmount(PaymentType.CARD));
    }

    @Test
    void getTotalAmount_paymentListIsEmpty_returnZero(){
        //Arrange
        //Act
        //Assert
        assertEquals(0.0f, pizzaServiceUnderTest.getTotalAmount(PaymentType.CARD));
    }

    @Test
    void getTotalAmount_invalidPaymentType_returnZero(){
        //Arrange
        //Act
        //Assert
        assertEquals(0.0f, pizzaServiceUnderTest.getTotalAmount(null));
    }

    @Test
    void getTotalAmount_anyPaymentsWithGivenPaymentType_returnZero(){
        //Arrange
        payRepo.add(new Payment(3, PaymentType.CASH, 24.01f));
        //Act
        //Assert
        assertEquals(0.0f, pizzaServiceUnderTest.getTotalAmount(PaymentType.CARD));
    }

    @Test
    void getTotalAmount_onePaymentWithGivenPaymentType_returnSum(){
        //Arrange
        payRepo.add(new Payment(3, PaymentType.CASH, 24.01f));
        //Act
        //Assert
        assertEquals(24.01f, pizzaServiceUnderTest.getTotalAmount(PaymentType.CASH));
    }

    @Test
    void getTotalAmount_twoPaymentsWithGivenPaymentType_returnSum(){
        //Arrange
        payRepo.add(new Payment(3, PaymentType.CARD, 7.00f));
        payRepo.add(new Payment(2, PaymentType.CARD, 3.00f));
        //Act
        //Assert
        assertEquals(10.00f, pizzaServiceUnderTest.getTotalAmount(PaymentType.CARD));
    }

    @Test
    void getTotalAmount_nPayments_returnSum(){
        //Arrange
        payRepo.add(new Payment(1, PaymentType.CASH, 2.50f));
        payRepo.add(new Payment(2, PaymentType.CARD, 3.00f));
        payRepo.add(new Payment(3, PaymentType.CARD, 7.00f));
        payRepo.add(new Payment(4, PaymentType.CASH, 3.50f));
        payRepo.add(new Payment(5, PaymentType.CARD, 24.01f));
        //Act
        //Assert
        assertEquals(6.00f, pizzaServiceUnderTest.getTotalAmount(PaymentType.CASH));
    }
}