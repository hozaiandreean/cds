package pizzashop.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import pizzashop.controller.KitchenGUIController;

import java.io.IOException;
import java.util.Optional;

public class KitchenGUI {
    private KitchenGUIController kitchenGUIController = null;

    public KitchenGUI() {
        VBox vBoxKitchen = null;
        FXMLLoader fxmlLoader = new FXMLLoader();

        try {
            vBoxKitchen = fxmlLoader.load(getClass().getResource("/fxml/kitchenGUIFXML.fxml").openStream());
            kitchenGUIController = fxmlLoader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage stage = new Stage();
        stage.setTitle("Kitchen");
        stage.setResizable(false);
        stage.setOnCloseRequest(event -> {
            Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION, "Would you like to exit Kitchen window?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> result = exitAlert.showAndWait();
            if (result.get() == ButtonType.YES) {
                stage.close();
            }
            // consume event
            else if (result.get() == ButtonType.NO) {
                event.consume();
            } else {
                event.consume();
            }
        });
        stage.setAlwaysOnTop(false);
        stage.setScene(new Scene(vBoxKitchen));
        stage.show();
        stage.toBack();
    }

    public KitchenGUIController getKitchenGUIController() {
        return kitchenGUIController;
    }
}

