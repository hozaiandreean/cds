package pizzashop.observer;

public interface IObservable<E extends Event> {
    void addObserver(IObserver<E> e);

    void removeObserver(IObserver<E> e);

    void notifyObservers(E t);
}
