package pizzashop.observer;

public interface IObserver<E extends Event> {
    void ready(E e, int tableNo);
}
