package pizzashop.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pizzashop.model.MenuDataModel;
import pizzashop.observer.Event;
import pizzashop.observer.IObserver;
import pizzashop.service.PaymentAlert;
import pizzashop.service.PizzaService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class OrdersGUIController implements IObserver<Event> {

    @FXML
    private ComboBox<Integer> orderQuantity;
    @FXML
    private TableView<MenuDataModel> orderTable;
    @FXML
    private TableColumn<MenuDataModel, Integer> tableQuantity;
    @FXML
    protected TableColumn<MenuDataModel, String> tableMenuItem;
    @FXML
    private TableColumn<MenuDataModel, Double> tablePrice;
    @FXML
    private Label pizzaTypeLabel;
    @FXML
    private Button addToOrder;
    @FXML
    private Label orderStatus;
    @FXML
    private Button placeOrder;
    @FXML
    private Button orderServed;
    @FXML
    private Button payOrder;
    @FXML
    private Button newOrder;

    private List<String> orderList = FXCollections.observableArrayList();
    private List<Double> orderPaymentList = FXCollections.observableArrayList();

    public static double getTotalAmount() {
        return totalAmount;
    }

    public static void setTotalAmount(double totalAmount) {
        OrdersGUIController.totalAmount = totalAmount;
    }

    private PizzaService service;
    private int tableNumber;

    public ObservableList<String> observableList;
    private TableView<MenuDataModel> table = new TableView<>();
    private static double totalAmount;
    public boolean pizzaReady = false;

    public OrdersGUIController() {
    }

    public void setService(PizzaService service, int tableNumber) {
        this.service = service;
        this.tableNumber = tableNumber;
        initData();

    }

    private void initData() {
        ObservableList<MenuDataModel> menuData;
        menuData = FXCollections.observableArrayList(service.getMenuData());
        menuData.setAll(service.getMenuData());
        orderTable.setItems(menuData);

        //Controller for Place Order Button
        placeOrder.setOnAction(event -> {
            orderList = menuData.stream()
                    .filter(x -> x.getQuantity() > 0)
                    .map(menuDataModel -> menuDataModel.getQuantity() + " " + menuDataModel.getMenuItem())
                    .collect(Collectors.toList());
            if (orderList.isEmpty()) {
                Alert exitAlert = new Alert(Alert.AlertType.WARNING, "You can't place order!", ButtonType.OK);
                exitAlert.showAndWait();
                return;
            }
            observableList = FXCollections.observableList(orderList);
            KitchenGUIController.order.add("Table" + tableNumber + " " + orderList.toString());
            String hour = LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute();
            orderStatus.setText("Order placed at: " + hour);
        });

        //Controller for Order Served Button
        orderServed.setOnAction(event -> {
            if (pizzaReady) {
                String hour = LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute();
                orderStatus.setText("Served at: " + hour);
                pizzaReady = false;
            } else {
                Alert exitAlert = new Alert(Alert.AlertType.WARNING, "Pizza is not ready!", ButtonType.OK);
                exitAlert.showAndWait();
            }

        });

        //Controller for Pay Order Button
        payOrder.setOnAction(event -> {
            if (orderStatus.getText().contains("Served at: ")) {
                orderPaymentList = menuData.stream()
                        .filter(x -> x.getQuantity() > 0)
                        .map(menuDataModel -> menuDataModel.getQuantity() * menuDataModel.getPrice())
                        .collect(Collectors.toList());
                setTotalAmount(orderPaymentList.stream().mapToDouble(Double::doubleValue).sum());
                orderStatus.setText("Total amount: " + getTotalAmount());
                System.out.println("--------------------------");
                System.out.println("Table: " + tableNumber);
                System.out.println("Total: " + getTotalAmount());
                System.out.println("--------------------------");
                PaymentAlert pay = new PaymentAlert(service);
                pay.showPaymentAlert(tableNumber, getTotalAmount());
            } else {
                Alert exitAlert = new Alert(Alert.AlertType.WARNING, "You can't pay before the pizza is served!", ButtonType.OK);
                exitAlert.showAndWait();
            }
        });
    }

    public void initialize() {

        //populate table view with menuData from OrderGUI
        table.setEditable(true);
        tableMenuItem.setCellValueFactory(
                new PropertyValueFactory<>("menuItem"));
        tablePrice.setCellValueFactory(
                new PropertyValueFactory<>("price"));
        tableQuantity.setCellValueFactory(
                new PropertyValueFactory<>("quantity"));

        //bind pizzaTypeLabel and quantity combo box with the selection on the table view
        orderTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> pizzaTypeLabel.textProperty().bind(newValue.menuItemProperty()));

        //Populate Combo box for Quantity
        ObservableList<Integer> quantityValues = FXCollections.observableArrayList(0, 1, 2, 3, 4, 5);
        orderQuantity.getItems().addAll(quantityValues);
        orderQuantity.setPromptText("Quantity");

        //Controller for Exit table Button
        newOrder.setOnAction(event -> {
            if (orderStatus.getText().contains("Total amount: ")) {
                Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION, "Exit table?", ButtonType.YES, ButtonType.NO);
                Optional<ButtonType> result = exitAlert.showAndWait();
                if (result.get() == ButtonType.YES) {
                    Stage stage = (Stage) newOrder.getScene().getWindow();
                    stage.close();
                }
            } else {
                Alert exitAlert = new Alert(Alert.AlertType.WARNING, "You can't leave before you pay!", ButtonType.OK);
                exitAlert.showAndWait();
            }
        });
    }

    public void handleAddToOrder() {
        MenuDataModel selectedMenuDataModel = orderTable.getSelectionModel().getSelectedItem();
        if (selectedMenuDataModel != null && orderQuantity != null && orderQuantity.getValue() != null) {
            selectedMenuDataModel.setQuantity(orderQuantity.getValue());
        } else {
            Alert exitAlert = new Alert(Alert.AlertType.WARNING, "You can't add item!", ButtonType.OK);
            exitAlert.showAndWait();
        }
    }

    @Override
    public void ready(Event event, int tableNo) {
        if (this.tableNumber == tableNo) {
            pizzaReady = true;
        }
    }
}
