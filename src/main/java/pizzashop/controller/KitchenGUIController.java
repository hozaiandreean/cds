package pizzashop.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import pizzashop.observer.Event;
import pizzashop.observer.IObservable;
import pizzashop.observer.IObserver;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class KitchenGUIController implements IObservable<Event> {
    @FXML
    private ListView<String> kitchenOrdersList;
    @FXML
    public Button cook;
    @FXML
    public Button ready;

    protected static final ObservableList<String> order = FXCollections.observableArrayList();
    private Object selectedOrder;
    private String extractedTableNumberString = "";
    private int extractedTableNumberInteger;
    List<IObserver<Event>> clients = new ArrayList<>();

    //thread for adding data to kitchenOrderList
    public Thread addOrders = new Thread(
            () -> {
                while (true) {
                    Platform.runLater(() ->
                            kitchenOrdersList.setItems(order));
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        break;
                    }
                }
            });

    public void initialize() {
        //starting thread for adding data to kitchenOrderList
        addOrders.setDaemon(true);
        addOrders.start();
        //Controller for Cook Button
        cook.setOnAction(event -> {
            selectedOrder = kitchenOrdersList.getSelectionModel().getSelectedItem();
            if (selectedOrder != null && !selectedOrder.toString().contains(" COOKING STARTED AT: ")) {
                kitchenOrdersList.getItems().remove(selectedOrder);
                String hour = LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute();
                kitchenOrdersList.getItems().add(selectedOrder.toString()
                        .concat(" Cooking started at: ").toUpperCase()
                        .concat(hour));
            } else {
                Alert exitAlert = new Alert(Alert.AlertType.WARNING, "You can't cook!", ButtonType.OK);
                exitAlert.showAndWait();
            }
        });
        //Controller for Ready Button
        ready.setOnAction(event -> {
            selectedOrder = kitchenOrdersList.getSelectionModel().getSelectedItem();
            if (selectedOrder != null && selectedOrder.toString().contains(" COOKING STARTED AT: ")) {
                kitchenOrdersList.getItems().remove(selectedOrder);
                extractedTableNumberString = selectedOrder.toString().subSequence(5, 6).toString();
                extractedTableNumberInteger = Integer.valueOf(extractedTableNumberString);
                System.out.println("--------------------------");
                String hour = LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute();
                System.out.println("Table " + extractedTableNumberInteger + " ready at: " + hour);
                System.out.println("--------------------------");
                this.notifyObservers(Event.READY);
            } else {
                Alert exitAlert = new Alert(Alert.AlertType.WARNING, "You must cook before ready!", ButtonType.OK);
                exitAlert.showAndWait();
            }
        });
    }

    @Override
    public void addObserver(IObserver<Event> e) {
        clients.add(e);
    }

    @Override
    public void removeObserver(IObserver<Event> e) {
        System.out.println(clients.toString());
        clients.remove(e);
        System.out.println(clients.toString());
    }

    @Override
    public void notifyObservers(Event t) {
        for (IObserver<Event> observer : clients)
            observer.ready(t, extractedTableNumberInteger);
    }
}