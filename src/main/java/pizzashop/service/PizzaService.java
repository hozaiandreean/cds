package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.List;

public class PizzaService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo) {
        this.menuRepo = menuRepo;
        this.payRepo = payRepo;
    }

    public List<MenuDataModel> getMenuData() {
        return menuRepo.getMenu();
    }

    public List<Payment> getPayments() {
        return payRepo.getAll();
    }

    /**
     * @param table  int, from 1 to 8
     * @param type   PaymentType, with value CASH/CARD
     * @param amount double, a real number greater than 0
     * @throws RuntimeException if the value of table is not valid or amount is less than or equal to 0
     */
    public void addPayment(int table, PaymentType type, double amount) throws RuntimeException {
        if (table < 1 || table > 8) {
            throw new RuntimeException("Not a valid table!");
        }
        if (amount <= 0) {
            throw new RuntimeException("Not a valid amount!");
        }
        Payment payment = new Payment(table, type, amount);
        payRepo.add(payment);
    }

    public double getTotalAmount(PaymentType type) {
        double total = 0.0f;
        if (payRepo == null) return total;
        List<Payment> l = getPayments();
        if (l.isEmpty()) return total;
        if (type != PaymentType.CARD && type != PaymentType.CASH) return total;
        for (int i = 0; i < l.size(); i++) {
            if (l.get(i).getType().equals(type))
                total += l.get(i).getAmount();
        }
        return total;
    }

}